import coverage
import pytest

def test():
    cov = coverage.Coverage()
    cov.start()

    pytest.main()

    cov.stop()
    cov.save()

    cov.html_report()